﻿using Microservice.Transfer.Data.Context;
using Microservice.Transfer.Domain.Interfaces;
using Microservice.Transfer.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Transfer.Data.Repository
{
    public class TransferRepository : ITransferRepository
    {
        private readonly TransferDbContext _context;

        public TransferRepository(TransferDbContext context)
        {
            _context = context;
        }

        public IEnumerable<TransferLog> GetTransferLogs()
        {
            return _context.TransferLogs;
        }

        public void Add(TransferLog transferLog)
        {
            _context.TransferLogs.Add(new TransferLog
            {
                FromAccount = transferLog.FromAccount,
                ToAccount = transferLog.ToAccount,
                Amount = transferLog.Amount,
            });
            _context.SaveChanges();
        }
    }
}