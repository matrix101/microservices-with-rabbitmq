﻿CREATE TABLE [dbo].[TransferLogs] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [FromAccount] INT          NOT NULL,
    [ToAccount]   INT          NOT NULL,
    [Amount]      DECIMAL (18) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

