﻿using Microservice.Banking.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Banking.Domain.Interfaces
{
    public interface IAccountRepository
    {
        IEnumerable<Account> GetAccounts();
    }
}