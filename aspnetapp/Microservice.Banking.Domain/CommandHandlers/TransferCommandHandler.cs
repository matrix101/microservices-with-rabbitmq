﻿using MediatR;
using Microservice.Banking.Domain.Commands;
using Microservice.Banking.Domain.Events;
using RabbitMQ.Domain.Core.Bus;
using System.Threading;
using System.Threading.Tasks;

namespace Microservice.Banking.Domain.CommandHandlers
{
    public class TransferCommandHandler : IRequestHandler<CreateTransferCommand, bool>
    {
        private readonly IEventBus _bus;

        public TransferCommandHandler(IEventBus bus)
        {
            _bus = bus;
        }

        public Task<bool> Handle(CreateTransferCommand request, CancellationToken cancellationToken)
        {
            //Publish Event to Rabbit
            _bus.Publish(new TransferCreatedEvent(request.From, request.To, request.Amount));

            return Task.FromResult(true);
        }
    }
}