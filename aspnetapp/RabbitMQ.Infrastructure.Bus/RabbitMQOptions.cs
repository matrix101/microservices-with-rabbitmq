﻿namespace RabbitMQ.Infrastructure.Bus
{
    public class RabbitMQOptions
    {
        public const string Position = "RabbitMq";

        public string Hostname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
