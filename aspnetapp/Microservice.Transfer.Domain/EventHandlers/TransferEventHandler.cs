﻿using Microservice.Transfer.Domain.Events;
using Microservice.Transfer.Domain.Interfaces;
using Microservice.Transfer.Domain.Models;
using RabbitMQ.Domain.Core.Bus;
using System.Threading.Tasks;

namespace Microservice.Transfer.Domain.EventHandlers
{
    public class TransferEventHandler : IEventHandler<TransferCreatedEvent>
    {
        private readonly ITransferRepository _transferRepository;

        public TransferEventHandler(ITransferRepository transferRepository)
        {
            _transferRepository = transferRepository;
        }

        public Task Handle(TransferCreatedEvent @event)
        {
            _transferRepository.Add(new TransferLog
            {
                FromAccount = @event.From,
                ToAccount = @event.To,
                Amount = @event.Amount
            });

            return Task.CompletedTask;
        }
    }
}