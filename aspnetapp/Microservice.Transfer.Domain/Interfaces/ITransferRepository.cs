﻿using Microservice.Transfer.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Transfer.Domain.Interfaces
{
    public interface ITransferRepository
    {
        IEnumerable<TransferLog> GetTransferLogs();

        void Add(TransferLog transferLog);
    }
}