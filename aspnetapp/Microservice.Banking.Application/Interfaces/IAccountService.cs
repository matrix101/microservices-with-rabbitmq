﻿using Microservice.Banking.Application.Models;
using Microservice.Banking.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Banking.Application.Interfaces
{
    public interface IAccountService
    {
        IEnumerable<Account> GetAccounts();

        void Transfer(AccountTransfer accountTransfer);
    }
}