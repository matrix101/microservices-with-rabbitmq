﻿using Microservice.Banking.Application.Interfaces;
using Microservice.Banking.Application.Models;
using Microservice.Banking.Domain.Commands;
using Microservice.Banking.Domain.Interfaces;
using Microservice.Banking.Domain.Models;
using RabbitMQ.Domain.Core.Bus;
using System.Collections.Generic;

namespace Microservice.Banking.Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IEventBus _bus;

        public AccountService(IAccountRepository accountRepository, IEventBus bus)
        {
            _accountRepository = accountRepository;
            _bus = bus;
        }

        public IEnumerable<Account> GetAccounts()
        {
            return _accountRepository.GetAccounts();
        }

        public void Transfer(AccountTransfer accountTransfer)
        {
            var createTransferCommand = new CreateTransferCommand(
                accountTransfer.FromAccount,
                accountTransfer.ToAccount,
                accountTransfer.TransferAmount
            );

            _bus.SendCommand(createTransferCommand);
        }
    }
}