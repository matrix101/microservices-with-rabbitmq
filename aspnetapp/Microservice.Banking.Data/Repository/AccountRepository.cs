﻿using Microservice.Banking.Data.Context;
using Microservice.Banking.Domain.Interfaces;
using Microservice.Banking.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Banking.Data.Repository
{
    public class AccountRepository : IAccountRepository
    {
        private readonly BankingDbContext _context;

        public AccountRepository(BankingDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Account> GetAccounts()
        {
            return _context.Accounts;
        }
    }
}