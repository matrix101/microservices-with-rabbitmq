﻿CREATE TABLE [dbo].[Accounts] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [AccountType]    VARCHAR (255) NOT NULL,
    [AccountBalance] DECIMAL (18)  NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

