﻿using Microservice.Transfer.Domain.Models;
using System.Collections.Generic;

namespace Microservice.Transfer.Application.Interfaces
{
    public interface ITransferService
    {
        IEnumerable<TransferLog> GetTransferLogs();
    }
}