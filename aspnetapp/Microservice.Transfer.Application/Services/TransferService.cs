﻿using Microservice.Transfer.Application.Interfaces;
using Microservice.Transfer.Domain.Interfaces;
using Microservice.Transfer.Domain.Models;
using RabbitMQ.Domain.Core.Bus;
using System.Collections.Generic;

namespace Microservice.Transfer.Application.Services
{
    public class TransferService : ITransferService
    {
        private readonly ITransferRepository _transferRepository;
        private readonly IEventBus _bus;

        public TransferService(ITransferRepository transferRepository, IEventBus bus)
        {
            _transferRepository = transferRepository;
            _bus = bus;
        }

        public IEnumerable<TransferLog> GetTransferLogs()
        {
            return _transferRepository.GetTransferLogs();
        }

        public void Add(TransferLog transferLog)
        {
            _transferRepository.Add(transferLog);
        }
    }
}