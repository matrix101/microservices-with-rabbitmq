﻿using MediatR;
using Microservice.Banking.Application.Interfaces;
using Microservice.Banking.Application.Services;
using Microservice.Banking.Data.Context;
using Microservice.Banking.Data.Repository;
using Microservice.Banking.Domain.CommandHandlers;
using Microservice.Banking.Domain.Commands;
using Microservice.Banking.Domain.Interfaces;
using Microservice.Transfer.Application.Interfaces;
using Microservice.Transfer.Application.Services;
using Microservice.Transfer.Data.Context;
using Microservice.Transfer.Data.Repository;
using Microservice.Transfer.Domain.EventHandlers;
using Microservice.Transfer.Domain.Events;
using Microservice.Transfer.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Domain.Core.Bus;
using RabbitMQ.Infrastructure.Bus;

namespace RabbitMQ.Infrastructure.IoC
{
    public static class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Domain Bus
            services.AddSingleton<IEventBus, RabbitMQBus>(sp =>
            {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                var options = sp.GetRequiredService<IOptions<RabbitMQOptions>>();
                var logger = sp.GetRequiredService<ILogger<RabbitMQBus>>();
                return new RabbitMQBus(sp.GetService<IMediator>(), scopeFactory, options, logger);
            });

            //Subscriptions
            services.AddTransient<TransferEventHandler>();

            //Domain Events
            services.AddTransient<IEventHandler<TransferCreatedEvent>, TransferEventHandler>();

            //Domain Commands
            services.AddTransient<IRequestHandler<CreateTransferCommand, bool>, TransferCommandHandler>();

            //Application Services
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<ITransferService, TransferService>();

            //Data
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<ITransferRepository, TransferRepository>();

            services.AddTransient<BankingDbContext>();
            services.AddTransient<TransferDbContext>();
        }
    }
}